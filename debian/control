Source: fauxdacious
Section: sound
Priority: optional
Maintainer: Patrice Duroux <patrice.duroux@gmail.com>
Build-Depends:
 debhelper-compat (= 13),
 libdbus-1-dev,
 libglib2.0-dev,
 libgtk2.0-dev,
 libsdl2-dev,
 qtbase5-dev
Standards-Version: 4.6.2
Homepage: https://wildstar84.wordpress.com/fauxdacious/

Package: fauxdacious
Architecture: any
Depends:
 default-dbus-session-bus | dbus-session-bus | dbus-x11,
 gtk2-engines-pixbuf,
 libfauxdcore5 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fauxdacious-plugin-skins (>= ${source:Upstream-Version}),
 fauxdacious-plugin-gtkui (>= ${source:Upstream-Version}),
 fauxdacious-plugin-pulseaudio (>= ${source:Upstream-Version})
Suggests:
 fauxdacious-plugins-all (>= ${source:Upstream-Version})
Description: small and fast audio player which supports lots of formats
 Audacious is a fork of beep-media-player which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the core player and its localization.

Package: libfauxdcore5
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: fauxdacious core library
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the libraries of fauxdacious.

Package: libfauxdgui5
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: fauxdacious gui library
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the libraries of fauxdacious.

Package: libfauxdqt2
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: fauxdacious Qt library
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.

Package: libfauxdtag3
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: fauxdacious tag library
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the libraries of fauxdacious.

Package: libfauxdcore-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libfauxdcore5 (= ${binary:Version}),
 ${misc:Depends}
Description: fauxdacious core library development files
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the development libraries and header files
 required for developing components for fauxdacious.

Package: libfauxdgui-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libfauxdgui5 (= ${binary:Version}),
 ${misc:Depends}
Description: fauxdacious gui library development files
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the development libraries and header files
 required for developing components for fauxdacious.

Package: libfauxdqt-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libfauxdqt2 (= ${binary:Version}),
 ${misc:Depends}
Description: fauxdacious Qt library development files
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.

Package: libfauxdtag-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libfauxdtag3 (= ${binary:Version}),
 ${misc:Depends}
Description: fauxdacious tag library development files
 fauxdacious is a fork of audacious which supports Winamp skins
 and many codecs.
 .
 In the default install, the following codecs are supported:
 .
  * MP3
  * Ogg Vorbis / Theora
  * AAC and AAC+
  * FLAC
  * ALAC
  * Windows Media (WMA)
  * WAVE
 .
 Additionally, fauxdacious is extendable through plugins, and contains
 other useful features like LIRC support. Support for many more codecs
 can also be added through plugins.
 .
 This package contains the development libraries and header files
 required for developing components for fauxdacious.
